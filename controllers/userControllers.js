// [SECTION] DEPENDENCIES 
const User = require("../models/User");
const Product = require("../models/Product")
const Order = require("../models/Order")

const bcrypt = require("bcrypt");
const auth = require("../auth");


// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {

    console.log(req.body);
    
    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: hashedPW
    });

    newUser.save()
    .then(user => res.send(user))
    .catch(err => res.send(err));
};

// [SECTION] RETRIEVAL OF ALL USERS

module.exports.getAllUsers = (req, res) => {

    User.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

// [SECTION] LOGIN USER

module.exports.loginUser = (req, res) => {

    console.log(req.body);

    User.findOne({email: req.body.email})
    .then(foundUser => {

        if(foundUser === null){
            return res.send(false);

        } else {

            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(isPasswordCorrect);

            if(isPasswordCorrect){

                return res.send({accessToken: auth.createAccessToken(foundUser)})

            } else {

                return res.send(false)
            }
        }
    })
    .catch(err => res.send(err));
};

// [SECTION] GETTING SINGLE USER DETAILS

module.exports.getUserDetails = (req, res) => {

    console.log(req.user);

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

// [SECTION] CHECK IF EMAIL EXISTS

module.exports.checkEmailExists = (req,res) => {

    User.findOne({email: req.body.email})
    .then(result => {

        //console.log(result)

        if(result === null){
            return res.send(false);
        } else {
            return res.send(true)
        }

    })
    .catch(err => res.send(err));
}

// [SECTION] UPDATING USER DETAILS

module.exports.updateUserDetails = (req, res) => {
    
    console.log(req.body); //input for new values
    console.log(req.user.id); // check the logged in user's id

    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo
    }

    User.findByIdAndUpdate(req.user.id, updates, {new: true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));   
};

// [SECTION] UPDATE AN ADMIN

module.exports.updateAdmin = (req, res) => {

    console.log(req.user.id); // id of the logged in user

    console.log(req.params.id); // id of the user we want to update

    let updates = {

        isAdmin: true
    }

    User.findByIdAndUpdate(req.params.id, updates, {new:true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
};