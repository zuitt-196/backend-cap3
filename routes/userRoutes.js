// [SECTION] DEPENDENCIES
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// [SECTION] ROUTES

// register user [Section]
router.post('/', userControllers.registerUser);

// get all user [Section]
router.get('/', userControllers.getAllUsers);

//login user [Section]
router.post('/login', userControllers.loginUser);

// get user details [Section]
router.get('/getUserDetails', verify, userControllers.getUserDetails)

// check if email exists [Section]
router.post('/checkEmailExists',userControllers.checkEmailExists);

// updating user details [Section]
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

// set admin [Section]
router.patch('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

module.exports = router;
