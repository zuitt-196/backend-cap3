const express = require("express");
const router = express.Router();
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

const orderControllers = require("../controllers/orderControllers");


// Creating order [Section]
router.post('/',verify,orderControllers.addOrder);

// View Authenticated User Order [Section]
router.get('/getUserOrder',verify,orderControllers.getUserOrder);

// View All Orders [Section]
router.get('/showAllOrders',verify,verifyAdmin,orderControllers.showAllOrders)

// View A Specific User Order/s [Section]
router.get('/showUserOrder/:userId',verify,verifyAdmin,orderControllers.showUserOrder)

// View A Specific User Order [Section]
router.get('/showOrder/:orderId',verify,verifyAdmin,orderControllers.showOrder)

// View Products Per Order [Section]
router.get('/showProductsPerOrder/:orderId',verify,orderControllers.showProductsPerOrder)

module.exports = router;


